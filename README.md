# qommoning

qommoning - is an attempt of developing diagrammatic representation of some of the (non) obvious limitations and frustrations with Commons by queer and questioning people...
It is based on personal and shared experiences of in+outside of networks and enclosures and proposed as a tool to reflect on other Commons in arts&culture – and to do it as the Other.
It is proposed as a draft by Z.Blace and hosted in open repository of framasoft's GitLab instance / framagit.org, as well as in its etherpad instance / framapad.org 
with graphviz renders via Totalism 
https://e2h.totalism.org/e2h.php?link=annuel.framapad.org/p/qommoning
with 10minDescription_rawRec_20191110_atCollectiveConditions_byConstantVzw.org_tnxReniHofmüller.mp3
https://wolke.mur.at/index.php/s/npxN87g3a6F5X99


digraph G {
  rankdir=TD;

"Commoning?!." -> "in context&dynamics"
"in context&dynamics" -> "Arts (autonomous)" -> "bottom-up/away"
"in context&dynamics" -> "Institutions" -> "pro/against fixture"
"in context&dynamics" -> "CSOs" -> "porous in/ex-clusivity"
"in context&dynamics" -> "Academia" -> "elite/accessible enclosure"
"in context&dynamics" -> "..."

"Commoning?!." -> "as generating"
"as generating" -> "content"
"as generating" -> "resources"
"as generating" -> "processes"
"as generating" -> "communities"


"processes" -> "of commoning are:" -> "presented" -> "+performed?" ->
"+embodied?"
"+performed?" -> "...not."
"+embodied?" -> "as..."

"processes" ->  "of selecting are:"
"of selecting are:" -> "competitive+openness" -> "(open calls)"
"of selecting are:" -> "discreet+invites" -> "(compensated commissions)"
"of selecting are:" -> "peer+contributions" -> "(shared care&use)"

"processes" ->  "of naming/exposing:" -> "...to be developed."


}


digraph G {
  rankdir=LR;
"Qommoning?!." -> "in participation mode" -> "...to develop."
"Qommoning?!." -> "in/on proximity" -> "...to develop.."
"Qommoning?!." -> "at/with time" -> "...to develop..."
"Qommoning?!." -> "with values of:" -> "opacity"

"opacity" -> "informative (partial information present)"
"opacity" -> "representational (selected information serve as
symbolic/formal gesture)"
"opacity" -> "revealing (some things can be observed and justified)"
"opacity" -> "insightful (most can be accessed in informative way)"
"opacity" -> "inspirational (all data shared, relations explicit and
analyzed together)"
"opacity" -> "indoctrinative (full transparency abused against vulnerable)"

}